var talot = [];

function ajax(url, func) {
    var request;

    if (window.XMLHttpRequest) {
        request = new XMLHttpRequest();
    } else {
        request = new ActiveXObject("Microsoft.XMLHTTP");
    }

    request.onreadystatechange = function() {
        if (request.readyState == 4 && request.status == 200) {
            func(request.responseText);
        }
    }

    request.open('GET', url, true);
    request.send();
}

function naytaTalot() {
    for (var i = 0; i < talot.length; i++) {
        var taloDiv = document.createElement("div");
        var img = document.createElement("img");
        var heading = document.createElement("h2");
        var size = document.createElement("p");
        var description = document.createElement("p");
        var price = document.createElement("p");

        taloDiv.setAttribute("class", "talo");
        img.setAttribute("src", talot[i].kuva);
        description.setAttribute("class", "kuvaus");

        heading.appendChild(document.createTextNode(talot[i].osoite));
        size.appendChild(document.createTextNode(talot[i].koko));
        description.appendChild(document.createTextNode(talot[i].kuvaus));
        price.appendChild(document.createTextNode(talot[i].hinta));

        taloDiv.appendChild(img);
        taloDiv.appendChild(heading);
        taloDiv.appendChild(size);
        taloDiv.appendChild(description);
        taloDiv.appendChild(price);

        document.getElementById("talot").appendChild(taloDiv);
    }
}

function loadJSON() {
    ajax("talot.json", function(response) {
        var JSONObject = JSON.parse(response);
        talot = JSONObject.talot;
        naytaTalot();
    })
}
