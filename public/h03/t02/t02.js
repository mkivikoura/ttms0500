var names = [];
var selectedName = "";
var resultsElement = document.getElementById("searchResults");

function updateResults() {
    var list = document.getElementById("searchResults");
    list.innerHTML = "";

    for (i = 0; i < names.length; i++) {
        var item = document.createElement("li");

        if (selectedName == names[i]) {
            item.setAttribute("id", "selected");
        }

        var clickURL = "http://axe.kivikoura.fi/h03/ajax-suggest.php?q=" + names[i];
        var clickFunction = "window.open('" + clickURL + "')";
        item.setAttribute("onclick", clickFunction);

        item.appendChild(document.createTextNode(names[i]));
        list.appendChild(item);
    }
}

function updateSelection(key) {
    var oldElement = document.getElementById("selected");
    var newName = "";

    if (resultsElement.children[0] == null) {
        return;
    }

    if (oldElement == null) {
        if (key == "ArrowUp") {
            newName = resultsElement.children[resultsElement.children.length - 1].innerHTML;
        } else {
            newName = resultsElement.children[0].innerHTML;
        }
    } else if (key == "ArrowUp" && oldElement.previousElementSibling != null) {
        newName = oldElement.previousElementSibling.innerHTML;
    } else if (key == "ArrowDown" && oldElement.nextElementSibling != null) {;
        newName = oldElement.nextElementSibling.innerHTML;
    }

    selectedName = newName;

    updateResults();
}

function ajax(url, func) {
    var request;

    if (window.XMLHttpRequest) {
        request = new XMLHttpRequest();
    } else {
        request = new ActiveXObject("Microsoft.XMLHTTP");
    }

    request.onreadystatechange = function() {
        if (request.readyState == 4 && request.status == 200) {
            func(request.responseText);
        }
    }

    request.open('GET', url, true);
    request.send();
}

function parseResponseToArray(response) {
    var array = response.split("\t");
    for (i = 0; i < array.length; i++) {
        array[i] = array[i].trim();
    }
    return array;
}

function searchName(searchParameter, event) {
    if (event.key == "ArrowUp" || event.key == "ArrowDown") {
        updateSelection(event.key);
        return;
    } else if (event.key == "Escape") {
        names = "";
        document.getElementById("searchInput").value = "";
        updateResults();
        return;
    }
    var searchURL = "ajax-suggest.php";
    var requestURL = "";

    if (searchParameter == "") {
        return;
    }

    requestURL = searchURL + "?q=" + searchParameter;

    ajax(requestURL, function(response) {
        names = parseResponseToArray(response);
        updateResults();
    })
}
