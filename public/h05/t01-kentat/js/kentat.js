function initMap() {
    var map = new google.maps.Map($("#map"), {
        center: {lat: 62.2426034, lng: 25.7472567},
        zoom: 7
    });

    var contentString = "";
    var infoWindow = new google.maps.InfoWindow({
        content: contentString
    });

    $.ajax({
        url: "data/kentat.json",
        dataType: "json",
        mimeType: "application/json",
        cache: null
    }).done(function(data){
        $.each(data.kentat, function(index, kentta){
            var kenttaPos = {lat: kentta.lat, lng: kentta.lng};
            var marker = new google.maps.Marker({
                position: kenttaPos,
                map: map,
                title: kentta.Kentta,
                kuvaus: kentta.Kuvaus,
                tyyppi: kentta.Tyyppi,
                osoite: kentta.Osoite,
                puhelin: kentta.Puhelin,
                sahkoposti: kentta.Sahkoposti,
                web: kentta.Webbi
            });

            if (kentta.Tyyppi == "Kulta") {
                marker.setIcon('http://maps.google.com/mapfiles/ms/icons/yellow-dot.png');
            }
            else if (kentta.Tyyppi == "Etu") {
                marker.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png');
            }
            else if (kentta.Tyyppi == "Kulta/Etu") {
                marker.setIcon('http://maps.google.com/mapfiles/ms/icons/blue-dot.png');
            }
        });
    });
}

$(document).ready(initMap());
